<?php
require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$hewan = new Animal("shaun");

echo "Name : " . $hewan->name . "<br>"; // "shaun"
echo "Legs : " .$hewan->legs . "<br>"; // 2
echo "Cold blooded :".$hewan->cold_blooded . "<br> <br>"; // false

$sungokong = new Ape("Kera Sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold blooded :" . $sungokong->cold_blooded ."<br>";
echo $sungokong->lompat();

$kodok = new Frog("Buduk");

echo "Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->yell();

?>